<?php
// $Id$

/**
 * @file
 * Use user badges for profile pictures.
 */

/**
 * Implementation of hook_menu().
 */
function user_badges_avatar_menu() {
  $items = array();
  $items['admin/user/user_badges/avatar'] = array(
    'title' => 'Avatars',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('user_badges_avatar_admin_settings_form'),
    'access arguments' => array('manage badges'),
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * User badges avatar admin settings form.
 */
function user_badges_avatar_admin_settings_form(&$form_state) {
  if (!variable_get('user_pictures', 0)) {
    drupal_set_message(t('You need to enable picture support for user profiles.'), 'error');
    return array('user_badges_avatar_error' => array('#value' => t('Click !here to configure user settings.', array('!here' => l('here', 'admin/user/settings')))));
  }
  $form = array();
  $form['user_badges_avatar_filter'] = array(
    '#type' => 'radios',
    '#title' => t('Which badges can user use as avatar?'),
    '#options' => array(
      'all' => t('All'),
      'earned' => t('Earned (default)'),
    ),
    '#default_value' => variable_get('user_badges_avatar_filter', 'earned'),
  );
  $form['user_badges_avatar_upload'] = array(
    '#type' => 'radios',
    '#title' => t('Enable upload'),
    '#options' => array(
      1 => t('Leave enabled (default)'),
      0 => t('Disable'),
    ),
    '#default_value' => variable_get('user_badges_avatar_upload', 1),
  );
  return system_settings_form($form);
}

/**
 * Implementation of hook_form_alter().
 */
function user_badges_avatar_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'user_profile_form':
      $account = user_load($form['#uid']);
      $options = array('none' => t('None (use uploaded image, if any)'));
      // Disable picture delete if the image is a badge
      if (user_badges_avatar_is_badge($account->uid)) {
        unset($form['picture']['picture_delete']);
      }
      // Disable core picture upload, if configured
      if (!variable_get('user_badges_avatar_upload', 1)) {
        unset($form['picture']['picture_upload']);
        $options['none'] = t('None');
      }
      // Select user badges based on configuration
      switch (variable_get('user_badges_avatar_filter', 'earned')) {
        case 'all':
          $badges = user_badges_get_badges('all');
          break;
        case 'earned':
          $badges = user_badges_get_badges($account->uid);
          break;
      }
      // Load the themed badges and add them as options
      module_load_include('inc', 'user_badges', 'user_badges.admin');
      $images = user_badges_image_selects();
      foreach ($badges as $badge) {
        if(array_key_exists($badge->image, $images)) {
          $options[$badge->bid] = $images[$badge->image];
        }
      }
      $form['picture']['user_badges'] = array(
        '#type' => 'radios',
        '#title' => t('User badges'),
        '#options' => $options,
        '#default_value' => $account->user_badges,
      );
      array_unshift($form['#validate'], 'user_badges_avatar_user_profile_form_validate');
      break;
  }
}

/**
 * Validate the user profile and set the badge as the user picture.
 */
function user_badges_avatar_user_profile_form_validate(&$form, &$form_state) {
  if ($form_state['values']['user_badges'] == 'none') {
    unset($form_state['values']['picture']);
  }
  // Unset old badge from profile before submitting to avoid deleting it
  if (user_badges_avatar_is_badge($form['#uid'])) {
    unset($form_state['values']['_account']->picture);
  }
  // Get the badge information from the badge id
  $badge = user_badges_get_badge($form_state['values']['user_badges']);
  // Set the path as the picture value (this is how user.module does it too)
  $form_state['values']['picture'] = $badge->image;
}

/**
 * Verify whether the user profile picture is a badge.
 */
function user_badges_avatar_is_badge($uid) {
  $account = user_load($uid);
  $badges = user_badges_get_badges($uid);
  foreach ($badges as $badge) {
    if ($account->picture == $badge->image) {
      return $badge->bid;
    }
  }
  return FALSE;
}
